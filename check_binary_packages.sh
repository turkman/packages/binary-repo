set -o pipefail
find -type f | grep "x86_64" | while read line ; do
    if ! unzip -l $line | grep data.tar &>/dev/null ; then
        echo $line
    fi
done