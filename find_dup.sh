last=""
lastsrc=""
find -iname '*.ymp' | sort -V -r | while read line ; do
    name=$(basename $line | cut -f1 -d_)
    if echo $line | grep "source.ymp" >/dev/null; then
        if [[ "$lastsrc" == "$name" ]] ; then
            echo rm -f $line
        fi
        lastsrc="$name"
    else
        if [[ "$last" == "$name" ]] ; then
            echo rm -f $line
        fi
        last="$name" 
    fi
done
