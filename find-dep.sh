#!/bin/bash
set -e
name="$1"
cur="$PWD"
find -type f -iname "*"$(uname -m)".ymp" | while read line ; do
    archive=$(realpath $line)
    mkdir /tmp/ymp-temp -p
    cd /tmp/ymp-temp
    unzip $archive data.tar.* &>/dev/null
    tar -xf data.tar.*
    find -type f | while read line ; do
        if ldd $line |& grep $name &>/dev/null ; then
            echo $archive
            break
        fi
    done
    rm -rf /tmp/ymp-temp
    cd $cur
done
