REPICENT=`gpg --list-keys | grep "^ " | tr -d " " | head -n 1`
index:
	ymp repo --index ./ --move --name="main" --verbose --allow-oem --gpg:repicent=$(REPICENT)
	bash find_dup.sh | sh -x
	ymp repo --index ./ --move --name="main" --verbose --allow-oem --gpg:repicent=$(REPICENT)

git: index
	git add .
	git commit
	git push
